/* stack.h -- interface for stack */
#ifndef _QUEUE_H_
#define _QUEUE_H_
#include <stdbool.h>

/* INSERT ITEM TYPE HERE */
typedef float Item;

#define MAXSTACK 10

typedef struct node
{
   Item item;
   struct node * next;
   struct node * previous;
} Node;

typedef struct stack
{
   Node * front;	/* pointer to front of the stack		*/
   Node * rear;		/* pointer to the rear of the stack		*/
   int number_items;
} Stack;

/* operation:		initialize the stack				*/
/* precondition:	pStack points to a stack			*/
/* postcondition:	stack is initialized to being empty		*/
void InitializeStack(Stack * pStack);

/* operation:		check if stack if full				*/
/* precondition:	pStack points to previously initialized stack	*/
/* postcondition:	returns True if stack is full, else False	*/
bool StackIsFull(const Stack * pStack);

/* operation:		check if stack is empty				*/
/* precondition:	pStack points to a previously initialized stack	*/
/* postcondition:	returns True if stack is empty, else False	*/
bool StackIsEmpty(const Stack * pStack);

/* operation:		determine number of items in stack		*/
/* precondition:	pStack points to previously initialized stack	*/
/* postcondition:	returns number of items in stack		*/
bool StackItemCount(const Stack * pStack);

/* operation:		push item into stack				*/
/* precondition:	pStack points to previously initialized stack	*/
/*			item is to be pushed at front of stack		*/
/* postcondition:	if stack is not empty, item is push into the	*/
/*			front of stack and function returns True;	*/
/*			otherwise, stack is unchanged and function	*/
/*			returns False.					*/
bool StackPush(Item *pItem, Stack * pStack);

/* operation:		pop item from the stack				*/
/* precondition:	pStack points to previosly initialized stack	*/
/* postcondition:	if stack is not empty, item at front of stack	*/
/*			is copied to *pItem and deleted from stack,	*/
/*			and function returns True; if the operation	*/
/*			empties the stack, the stack is reset to	*/
/*			empty. If the stack is empty to begin with,	*/
/*			stack is unchanged and function returns False	*/
bool StackPop(Item *pItem, Stack * pStack);

/* operation:		empty the stack					*/
/* precondition:	pStack points to previously initialized stack	*/
/* postcondition:	the stack is empty				*/
void EmptyTheStack(Stack * pStack);

#endif
